/**
 * Created by shahab on 10/7/15.
 */
module.exports = {
    CustomerService : require('./CustomerService'),
    DriverService : require('./DriverService'),
    AdminService : require('./AdminService'),
    AppVersionService : require('./AppVersionService')
};