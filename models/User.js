"use strict";

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define("User", {
    id: {type: DataTypes.INTEGER,primaryKey: true},
	userName: {type:DataTypes.STRING},
	userEmail:{type:DataTypes.STRING},
    userPassword:{type:DataTypes.STRING},
    phoneNumber: {type:DataTypes.STRING},
	createdOn: {type:DataTypes.DATE},
  });
    return User;
    };
