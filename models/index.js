"use strict";

var fs        = require("fs");
var path      = require("path");
var Sequelize = require("sequelize");
var env       = process.env.NODE_ENV || "development";
var config    = require(path.join(__dirname, '..', 'config', 'config.json'))[env];
var sequelize = new Sequelize(config.database, config.username, config.password, config);
var db        = {};

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js");
  })
  .forEach(function(file) {
    console.log("file read.....",file)
    var model = sequelize.import(path.join(__dirname, file));
    console.log("model output in index file.....",model)
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  console.log("db in index.....",db)
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});
console.log("sequalizer db.....",sequelize)
db.sequelize = sequelize;
console.log("sequalizer db.....",db.sequelize);
db.Sequelize = Sequelize;

module.exports = db;